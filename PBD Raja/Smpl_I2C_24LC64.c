/*---------------------------------------------------------------------------------------------------------*/
/*                                                                                                         */
/* Copyright (c) Nuvoton Technology Corp. All rights reserved.                                             */
/*                                                                                                         */
/*---------------------------------------------------------------------------------------------------------*/

#include <stdio.h>
#include "NUC1xx.h"
#include "Driver\DrvSYS.h"
#include "Driver\DrvGPIO.h"
#include "LCD_Driver.h"
#include "EEPROM_24LC64.h"
#include "Driver\DrvI2C.h"
#include "Seven_Segment.h"
#define STRIP 16
#define NONAKTIF -1
#define true 1
#define false 0

int offsetPrint;
int KunciJali[255][4]; //diisi oleh address offset
int JmlKey;
int Offset;
int page;
int MaxRow;

void delay_loop(int s)
{
 uint32_t i,j;
	for(i=0;i<3;i++)	
	{
		for(j=0;j<s;j++);
    }
 }

void delay(void)
{
int j;
   for(j=0; j<1000; j++);
}

uint8_t scan_key(void)
{
uint8_t act[4]={0x3b, 0x3d, 0x3e};    
uint8_t i,temp,pin;

for(i=0;i<3;i++)
{
       temp=act[i];
	   for(pin=0;pin<6;pin++)
	    {
	  	if((temp&0x01)==0x01)
        DrvGPIO_SetBit(E_GPA,pin);
		else
		DrvGPIO_ClrBit(E_GPA,pin);		  
		temp=temp>>1; 	  
	    }							    
	    delay();
		if(DrvGPIO_GetBit(E_GPA,3)==0) 
		return (i+1);
}

for(i=0;i<3;i++)
{
       temp=act[i];
	   for(pin=0;pin<6;pin++)
	    {
	  	if((temp&0x01)==0x01)
        DrvGPIO_SetBit(E_GPA,pin);
		else
		DrvGPIO_ClrBit(E_GPA,pin);		  
		temp=temp>>1; 	  
	    }							    
	    delay();
		if(DrvGPIO_GetBit(E_GPA,4)==0) 
		return (i+4);
}

for(i=0;i<3;i++)
{
       temp=act[i];
	   for(pin=0;pin<6;pin++)
	    {
	  	if((temp&0x01)==0x01)
        DrvGPIO_SetBit(E_GPA,pin);
		else
		DrvGPIO_ClrBit(E_GPA,pin);		  
		temp=temp>>1; 	  
	    }							    
	    delay();
		if(DrvGPIO_GetBit(E_GPA,5)==0) 
		return (i+7);
}

 return 0;

}

void addKey(int[4],int);
void initPrint();
void printDelMsg()
{
	print_lcd(0,"EEPROM akan       "); 
	print_lcd(1,"didelete			     ");  
	print_lcd(2,"							     ");  
	print_lcd(3,"								   ");
}

void processKey(int* iterasi, int* aktif, int temp, int *counter, int num[4], int* hasil, int isGanti) {
int k;
if (temp == 7) {
	//Backspace
	*aktif = -1;
	*iterasi = 0;
	if (*counter > 0) {	
		for (k = 0; k < *counter; k++) {
			num[k] = num[k+1];
		}
		num[*counter-1] = STRIP;		
		(*counter)--;
	}
} else if (temp == 8){
		//disini nyimpen ke EEPROM dan view
		//if (temp == *aktif)
		//{
			addKey(num,(*counter));
			*iterasi = 0;
			for (k = 0; k < 4; k++) {
				num[k] = STRIP;
			}
			*counter = 0;
			(*aktif) = -1;
		//}
		//else
		//{
		//	DrvGPIO_InitFunction(E_FUNC_I2C1);  	
		//	Write_24LC64(0x00000000+0,0);
		//	(*aktif) = temp;
		//}
} else {
		if (temp == *aktif && !isGanti) {
		(*iterasi)++;
		if (temp == 1) {
		if (*iterasi % 2 == 0) {
		*hasil = 0;
		} else {
		*hasil = 1;
		}
	} else if (temp == 2) {
		if (*iterasi % 3 == 0) {
			*hasil = 2;
		} else if (*iterasi % 3 == 1) {
			*hasil = 3;
		} else if (*iterasi % 3 == 2) {
			*hasil = 4;
		}
	} else if (temp == 3) {
		if (*iterasi % 3 == 0) {
			*hasil = 5;
		} else if (*iterasi % 3 == 1) {
			*hasil = 6;
		} else if (*iterasi % 3 == 2) {
			*hasil = 7;
		}
	} else if (temp == 4) {
		if (*iterasi % 3 == 0) {
			*hasil = 8;
		} else {
			*hasil = 9;
		}
	} else if (temp == 5) {
		if (*iterasi % 3 == 0) {
			*hasil = 10;
		} else if (*iterasi % 3 == 1) {
			*hasil = 11;
		} else if (*iterasi % 3 == 2) {
			*hasil = 12;
		}
	} else if (temp == 6) {
		if (*iterasi % 3 == 0) {
			*hasil = 13;
		} else if (*iterasi % 3 == 1) {
			*hasil = 14;
		} else if (*iterasi % 3 == 2) {
			*hasil = 15;
		}
		}
	} else {
		if (*counter < 4) {
				*aktif = temp;
				*iterasi = 0;
				for (k = *counter; k > 0; k--) {
					num[k] = num[k-1];
				}
				if (temp == 1) {
					*hasil = 0;
				} else if (temp == 2) {
					*hasil = 2;
				} else if (temp == 3) {
					*hasil = 5;
				} else if (temp == 4) {
					*hasil = 8;
				} else if (temp == 5) {
					*hasil = 10;
				} else if (temp == 6) {
					*hasil = 13;
				}
					(*counter)++;
			}
		}
		num[0] = *hasil;
	}
}

void addKey(int num[4], int counter) //counter pasti 4
{
	int i;
	for (i=counter;i<4;i++)
	{
		num[i] = 18;
	}
	
	for (i=0;i<4;i++)
	{
		DrvGPIO_InitFunction(E_FUNC_I2C1);
		Write_24LC64(0x00000004+Offset,num[4-i-1]);
		KunciJali[JmlKey][i] = Offset;
		Offset++;
	}
	
	JmlKey++;
	DrvGPIO_InitFunction(E_FUNC_I2C1);
	Write_24LC64(0x00000000+0,JmlKey);
	DrvGPIO_InitFunction(E_FUNC_I2C1);
	Write_24LC64(0x00000000+1,Offset);
}

void initPrint()
{
	int i;
	for (i=0;i<4;i++)
		print_lcd(i,"                 ");
}

void printLCD(char print[16][4])
{
	int i;
	for (i=0;i<4;i++)
		print_lcd(i,print[i]);
}

void printError()
{
	print_lcd(0,"ERROR             "); 
	print_lcd(1,"Tidak ada key     ");  
	print_lcd(2,"yang tersimpan    ");  
	print_lcd(3,"pada EEPROM       ");
}

void isiString(char ret[16], char key[4], int offset)
{
	int i;
	int k = 0;
	for (i=0;i<16;i++)
	{
		if ((i >= offset) && (i < (offset+4)))
		{
			ret[i] = key[k];
			k++;
		}
	}
}

void initialChar(char print[4][16])
{
		int i,j;
		for (i=0;i<4;i++)
		{
				for (j=0;i<16;j++)
				{
					print[j][i] = ' ';
				}
		}
}

void initialLCD(char print[150][16])
{
	int i,j;
	for (i=0;i<150;i++)
	{
		for (j=0;j<16;j++)
		{
			print[i][j] = ' ';
		}
	}
}

void printKey(int idxKey, int page)
{ //perbesar IdxPrint (dari 30->150)
	uint32_t i2cdata=0;
	int max;
	int i,j,k;
	char print[150][16];
	int offsetChar=0;
	int offset=idxKey;
	
	offsetPrint = 0;	
	initialLCD(print);
	for (i=idxKey;i<JmlKey;i++) //JmlKey+1
	{				
		for (j=0;j<4;j++)
		{
			DrvGPIO_InitFunction(E_FUNC_I2C1);  	
			i2cdata= Read_24LC64(0x00000004+(offset+1));
			if (i2cdata != 0x00000000+18)
					sprintf(print[offsetPrint]+offsetChar,"%x",i2cdata);
			else
				sprintf(print[offsetPrint]+offsetChar,"%c",' ');
			
			offset++;
			offsetChar++;
		}		
		sprintf(print[offsetPrint]+offsetChar,"%c",' ');
		offsetChar++;
		
		if (((i+1) % MaxRow == 0) && (i!=JmlKey-1))
		{
			offsetPrint++;
			offsetChar = 0;
		}
	}
	
	if (page > 0)
	{
		max = offsetPrint - 4;
	}
	
	if (offsetPrint > 3) max = 3;
	else max = offsetPrint;
	
	for (k=0;k<=max;k++)
			print_lcd(k,print[k+(page*4)]);
}

int main(void)
{
	  uint32_t i;
		int j;
		int num[4];
		int offsetKey=0;
		int counter = 0;
		int flags = 0;
		int aktif = -1;
		int iterasi = 0;
		int hasil;
		int time = 0;
		int isPage = false;
		int AwalPage;
		int isHapus = false;
		
		unsigned char temp;
	
		MaxRow = 3; //set manual
		page = 0;		
		JmlKey = 0; //initial
		Offset = 1; //initial
	/* Unlock the protected registers */	
	UNLOCKREG();
   	/* Enable the 12MHz oscillator oscillation */
	DrvSYS_SetOscCtrl(E_SYS_XTL12M, 1);
 
     /* Waiting for 12M Xtal stalble */
    SysTimerDelay(5000);
 
	/* HCLK clock source. 0: external 12MHz; 4:internal 22MHz RC oscillator */
	DrvSYS_SelectHCLKSource(0);		
    /*lock the protected registers */
	LOCKREG();				

	DrvSYS_SetClockDivider(E_SYS_HCLK_DIV, 0); /* HCLK clock frequency = HCLK clock source / (HCLK_N + 1) */

	Initial_pannel();  //call initial pannel function
	clr_all_pannal();
	
	print_lcd(0, "PBD Raja");
	print_lcd(1, "Key  ");
	print_lcd(2, "Open Chest ");	  
	print_lcd(3, "press key1-key9");
	
	//initial key board
	for(i=0;i<6;i++)		
		DrvGPIO_Open(E_GPA, i, E_IO_QUASI);
	
	num[0] = STRIP;
	num[1] = STRIP;
	num[2] = STRIP;
	num[3] = STRIP;

	DrvGPIO_InitFunction(E_FUNC_I2C1);  	
	JmlKey = Read_24LC64(0x00000000+0);
	DrvGPIO_InitFunction(E_FUNC_I2C1);
	Offset = Read_24LC64(0x00000000+1);
	
	while(true)
	{
	   temp=scan_key();
		 if (aktif != 9)
		 {
			for (j = 0; j < 4; j++) {
				close_seven_segment();
				show_seven_segment(j,num[j]);
				delay_loop(1500);
			}
		}
		 else
		 {
			 close_seven_segment();
				show_seven_segment(0,page);
				delay_loop(1500);
		 }
		delay_loop(1000);
		time++;
		if (time > 100) {
			flags = true;
		}
		
		if(temp==1)
		{
			print_lcd(0,"Tombol 0-1      ");
		  print_lcd(1,"ditekan         ");
		  print_lcd(2,"                ");
			print_lcd(3,"                ");
			time = 0;
			processKey(&iterasi, &aktif, temp, &counter, num, &hasil, flags);
			flags = false;
			isPage = false;
			isHapus = false;
			page=0;
		}
		if(temp==2)
		{
				print_lcd(0,"Tombol 2-3-4    ");
				print_lcd(1,"ditekan         ");
				print_lcd(2,"                ");
				print_lcd(3,"                ");
				time = 0;
				processKey(&iterasi, &aktif, temp, &counter, num, &hasil, flags);
				flags = false;
				isPage = false;
				isHapus = false;
				page=0;
		}
		if(temp==3)
		{
				print_lcd(0,"Tombol 5-6-7    ");
				print_lcd(1,"ditekan         ");
				print_lcd(2,"                ");
				print_lcd(3,"                ");
				time = 0;
				processKey(&iterasi, &aktif, temp, &counter, num, &hasil, flags);
				flags = false;
				isPage = false;
				isHapus = false;
				page=0;
		}
		if(temp==4)
		{
			print_lcd(0,"Tombol 8-9     ");
		  print_lcd(1,"ditekan        ");
		  print_lcd(2,"               ");
			print_lcd(3,"               ");
			processKey(&iterasi, &aktif, temp, &counter, num, &hasil, flags);
			time = 0;
			flags = false;
			isPage = false;
			isHapus = false;
			page=0;
		}
		if(temp==5)
		{
			print_lcd(0,"Tombol A-B-C   ");
		  print_lcd(1,"ditekan        ");
		  print_lcd(2,"               ");
			print_lcd(3,"               ");
			processKey(&iterasi, &aktif, temp, &counter, num, &hasil, flags);
			time = 0;
			flags = false;
			isPage = false;
			isHapus = false;
			page=0;
		}
		if(temp==6)
		{
			print_lcd(0,"Tombol D-E-F   ");
		  print_lcd(1,"ditekan        ");
		  print_lcd(2,"               ");
			print_lcd(3,"               ");
			processKey(&iterasi, &aktif, temp, &counter, num, &hasil, flags);
			time = 0;
			flags = false;
			isPage = false;
			isHapus = false;
			page=0;
	  }
		if(temp==7)
		{
			print_lcd(0,"Backspace      ");
			print_lcd(1,"               ");
			print_lcd(2,"               ");
			print_lcd(3,"               ");
			processKey(&iterasi, &aktif, temp, &counter, num, &hasil, flags);
			time = 0;
			flags = false;
			isPage = false;
			isHapus = false;
			page=0;
		}
		if(temp==8)
		{
			if (!isHapus)
			{
				processKey(&iterasi, &aktif, temp, &counter, num, &hasil, flags);
				print_lcd(0,"Kunci telah    ");
				print_lcd(1,"disimpan       ");
				print_lcd(2,"               ");
				print_lcd(3,"               ");
				isHapus = true;
			}
			else
			{
				print_lcd(0,"Kunci telah    ");
				print_lcd(1,"dihapus       ");
				print_lcd(2,"               ");
				print_lcd(3,"               ");
				DrvGPIO_InitFunction(E_FUNC_I2C1);  	
				Write_24LC64(0x00000000+0,0);
				DrvGPIO_InitFunction(E_FUNC_I2C1);
				Write_24LC64(0x00000000+1,1);
				JmlKey = 0;
				Offset = 1;
			}
			time = 0;
			flags = false;
			isPage = false;
			page=0;
		}
		if(temp==9)
		{
			//LOAD LCD EEPROM
			if (JmlKey <= 0)
			{
				printError();
			}
			else
			{
				if (JmlKey <= 0)
				{
					printError();
				}
				else
				{
					aktif = 9;
					initPrint();
					printKey(0,page);
					if ((page*4) >= offsetPrint) page=0;
					else page++;
				}		
			}		
			time = 0;
			flags = false;
			isHapus = false;
		}
	}	  		
}


